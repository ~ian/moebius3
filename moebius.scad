// -*- C -*-

include <moebius-core.scad>

// settings etc.

pin_stemwidth = 1.5;
pin_stemlen = 1.0;
pin_headwidth = 3.0;
pin_headheight = 0.5;
pin_slot_xslop = 0.25;

pin_yadj_range = [ -0.50, +0.25 ];
pin_yslop = 0.25;

pin_pin_xslop = 0.00;
pin_pin_yadj_range = pin_yadj_range + [ 0.00, 0.00 ];

pin_height = 5;

// computed

sliceat = moebiuscore_sliceat * moebiuscore_nomsize;
pin_slopeheight = (pin_headwidth - pin_stemwidth)/2;

module TopSlice(){
  translate([0,0, 200-sliceat])
    cube(center=true, 400);
}

module MainBody(){
  intersection(){
    MoebiusCore();
    TopSlice();
  }
}

module HalfPinHeadProfile(xslop, yadj, lenadj) {
  sw = pin_stemwidth + xslop*2;
  tw = pin_headwidth + xslop*2;
  th = pin_headheight + lenadj - yadj;
  hull(){
    translate([ -tw/2,
		pin_stemlen/2 + yadj + pin_slopeheight ])
      square([ tw, th ]);
    translate([ -sw/2,
		pin_stemlen/2 + yadj ])
      square([ sw, th ]);
  }
}

module PinStemProfile(xslop, yadj) {
  sl = pin_stemlen + pin_headheight/2 + max(yadj * 2, 0);
  square(center=true,
	 [ pin_stemwidth + xslop*2,
	   sl ]);
}

module PinExtrudePortion(zi,zrange){
  translate([0,0,zrange[zi]])
    mirror([0,0,zi])
    linear_extrude(height=0.1)
    children(0);
}

module PinExtrude(zrange,
		  xslop=0,
		  yadjs=[[0,0], [0,0]], // [top[], bottom[]]
		  lenadjs=[0,0]){ // in each case: zrange[0],zrange[1]
  for (topboti=[0,1]) {
    mirror([0,topboti,0]){
      hull(){
	for (zi=[0,1])
	  PinExtrudePortion(zi,zrange)
	    HalfPinHeadProfile(xslop, yadjs[topboti][zi], lenadjs[zi]);
      }
    }
  }
  hull(){
    for (zi=[0,1])
      PinExtrudePortion(zi,zrange)
	PinStemProfile(xslop, max(yadjs[zi][0], yadjs[zi][1]));
  }
}

module PinCutout(){
  PinExtrude([-10,10],
	     xslop= pin_slot_xslop,
	     yadjs = [ [0,0],
		       [1,1] * -pin_stemlen/2 ],
	     // bigger gap in -ve y direction
	     lenadjs = [1,1] * (pin_yslop + pin_yadj_range[1]) );
}

module Pin(){
  PinExtrude([-0.5, 0.5] * pin_height,
	     xslop = -pin_pin_xslop,
	     yadjs = [pin_pin_yadj_range, pin_pin_yadj_range],
	     lenadjs = [1,1] * pin_yadj_range[1]
	     );
}

module PlacePins(pins=[0,1]){
  for (i=pins) {
    translate( moebius_pin_locns[i] * moebiuscore_nomsize ) {
      multmatrix(moebius_pin_matrix[i])
	children(0);
    }
  }
}

module PinDemo(){
  PlacePins()
    cube([1,4,9]);
}

module Top(){ ////toplevel
  difference(){
    intersection(){
      MoebiusCore();
      TopSlice();
    }
    PlacePins(){
      PinCutout();
    }
  }
}

module Bottom(){ ////toplevel
  difference(){
    MoebiusCore();
    TopSlice();
    PlacePins()
      mirror([0,1,0]) PinCutout();
  }
}

module Demo(){ ////toplevel
  Bottom();
  %Top();
  color("blue") PlacePins([0]) Pin();
}

module Kit(){ ////toplevel
  off = moebiuscore_nomsize*2.5;
  pt = pin_stemlen/2 + pin_headheight
    + pin_pin_yadj_range[1] + pin_slopeheight;

  translate([0,0,sliceat])
    Top();
  translate([ 0, -off, 0 ])
    rotate([0,180,0])
    translate([0,0,sliceat])
    Bottom();
  for (d = [0,1] * pin_height * 2)
    translate([ off, d, pt ])
    rotate([90,0,0])
    Pin();
}

module TestKit(){ ////toplevel
  intersection(){
    Kit();
    cube([500,500,15], center=true);
  }
}

//MainBody();
//%PinDemo();
//PinExtrude([-2,10]);
//PinCutout();
//Pin();
//%PinCutout();
//Top();
//Bottom();
//Kit();
//Demo();
//TestKit();
