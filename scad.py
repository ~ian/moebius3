
from __future__ import print_function

class ScadObject:
  def __init__(so):
    so._points = []
    so._point_indices = {}
    so._triangles = []

  def writeout_core(so, scalefactor=1):
    if not so._points: return
    print('scale(%s) polyhedron(points=[' % scalefactor)
    for p in so._points: print(p, ',')
    print('],faces=[')
    for t in so._triangles: print(repr(t), ',')
    so._points = None
    print('],convexity=10);')

  def writeout(so, objname, scalefactor=1):
    print('module %s(){' % objname)
    so.writeout_core(scalefactor)
    print('}')

  def _point(so, p):
    l = list(p)
    s = repr(l)
    try:
      ix = so._point_indices[s]
    except KeyError:
      ix = len(so._points)
      so._points.append(s)
      so._point_indices[s] = ix
    return ix

  def triangle(so, a,b,c):
    ''' a b c  are clockwise from inside '''
    so._triangles.append([ so._point(p) for p in (a,b,c) ])

  def quad(so, cnrs):
    ''' cnrs[0] [1] [3] [2] are clockwise from inside '''
    so.triangle(cnrs[0], cnrs[1], cnrs[3])
    so.triangle(cnrs[0], cnrs[3], cnrs[2])

  def rquad(so, cnrs):
    ''' cnrs[0] [1] [3] [2] are anticlockwise from inside '''
    so.triangle(cnrs[0], cnrs[3], cnrs[1])
    so.triangle(cnrs[0], cnrs[2], cnrs[3])

