
from __future__ import print_function

from bezier import BezierSegment
from moedebug import *
from moenp import *

class DoubleCubicBezier():
  def __init__(db, cp):
    single = BezierSegment(cp)
    midpoint = np.array(single.point_at_t(0.5))
    mid_dirn = single.point_at_t(0.5 + 0.001) - midpoint
    mid_dirn /= np.linalg.norm(mid_dirn)
    ocp_factor = 0.5
    mid_scale = ocp_factor * 0.5 * (np.linalg.norm(cp[1] - cp[0]) +
                                    np.linalg.norm(cp[3] - cp[2]))
    db.b0 = BezierSegment([ cp[0],
                            cp[1] * ocp_factor + cp[0] * (1-ocp_factor),
                            midpoint - mid_dirn * mid_scale,
                            midpoint ])
    db.b1 = BezierSegment([ midpoint,
                            midpoint + mid_dirn * mid_scale,
                            cp[2] * ocp_factor + cp[3] * (1-ocp_factor),
                            cp[3] ])
  def point_at_t(db, t):
    if t < 0.5:
      return db.b0.point_at_t(t*2)
    else:
      return db.b1.point_at_t(t*2 - 1)

class DiscreteBezier():
  def __init__(b, cp, nt, bezier_constructor=DoubleCubicBezier):
    b.nt = nt
    b._t_vals = np.linspace(0, 1, b.nt+1)
    b._continuous_bezier = bezier_constructor(cp)
  def point_at_it(b, it):
    t = b._t_vals[it]
    return b._continuous_bezier.point_at_t(t)

