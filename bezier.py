# Copied from
#   /usr/lib/python2.7/dist-packages/matplotlib/bezier.py
# in Debian's python-matplotlib 2.0.0+dfsg1-2 (amd64)
# and trimmed to have only BezierSegment

# Copyright: Copyright (c) 2002 - 2012 John Hunter, Darren Dale, Eric Firing, Michael Droettboom and the matplotlib development team; 2012 - 2016 The matplotlib development team
#
# 1. This LICENSE AGREEMENT is between the Matplotlib Development Team
# ("MDT"), and the Individual or Organization ("Licensee") accessing and
# otherwise using matplotlib software in source or binary form and its
# associated documentation.
# .
# 2. Subject to the terms and conditions of this License Agreement, MDT
# hereby grants Licensee a nonexclusive, royalty-free, world-wide license
# to reproduce, analyze, test, perform and/or display publicly, prepare
# derivative works, distribute, and otherwise use matplotlib
# alone or in any derivative version, provided, however, that MDT's
# License Agreement and MDT's notice of copyright, i.e., "Copyright (c)
# 2012- Matplotlib Development Team; All Rights Reserved" are retained in
# matplotlib  alone or in any derivative version prepared by
# Licensee.
# .
# 3. In the event Licensee prepares a derivative work that is based on or
# incorporates matplotlib or any part thereof, and wants to
# make the derivative work available to others as provided herein, then
# Licensee hereby agrees to include in any such work a brief summary of
# the changes made to matplotlib .
# .
# 4. MDT is making matplotlib available to Licensee on an "AS
# IS" basis.  MDT MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
# IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, MDT MAKES NO AND
# DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
# FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF MATPLOTLIB
# WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
# .
# 5. MDT SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF MATPLOTLIB
#  FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR
# LOSS AS A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING
# MATPLOTLIB , OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF
# THE POSSIBILITY THEREOF.
# .
# 6. This License Agreement will automatically terminate upon a material
# breach of its terms and conditions.
# .
# 7. Nothing in this License Agreement shall be deemed to create any
# relationship of agency, partnership, or joint venture between MDT and
# Licensee.  This License Agreement does not grant permission to use MDT
# trademarks or trade name in a trademark sense to endorse or promote
# products or services of Licensee, or any third party.
# .
# 8. By copying, installing or otherwise using matplotlib ,
# Licensee agrees to be bound by the terms and conditions of this License
# Agreement.

import six
import numpy as np
import warnings

class BezierSegment(object):
    """
    A simple class of a N-dimensional bezier segment
    """

    # Higher order bezier lines can be supported by simplying adding
    # corresponding values.
    _binom_coeff = {1: np.array([1., 1.]),
                    2: np.array([1., 2., 1.]),
                    3: np.array([1., 3., 3., 1.])}

    def __init__(self, control_points):
        """
        *control_points* : location of contol points. It needs have a
         shpae of n * D, where n is the order of the bezier line
         and D is the dimension (the length of each control point
         tuple). 1<= n <= 3 is supported.
        """
        _o = len(control_points)
        dim = len(control_points[0])
        self._orders = np.arange(_o)
        _coeff = BezierSegment._binom_coeff[_o - 1]

        _control_points = np.asarray(control_points)
        xyz = [_control_points[:, i] for i in range(0, dim)]

        self._pxyz = [xx * _coeff for xx in xyz]

    def point_at_t(self, t):
        "evaluate a point at t"
        one_minus_t_powers = np.power(1. - t, self._orders)[::-1]
        t_powers = np.power(t, self._orders)

        tt = one_minus_t_powers * t_powers
        _xyz = [sum(tt * px) for px in self._pxyz]

        return tuple(_xyz)
