// -*- C -*-

include <moebius-core.scad>
include <commitid.scad>

sz = moebiuscore_nomsize;

lsz = [sz*2.4, 6, sz*0.5];

difference(){
  union(){
    cube([5, sz*1.7,   sz*0.5], center=true);
    cube(lsz, center=true);
  }
  rotate([0,0,-25])
    translate(sz * [0,0,1.9])
    hull() MoebiusCore();
  translate(-lsz/2)
    Commitid_BestCount_M([ lsz[0], lsz[1] ]);
}
