/*
 */

#include <math.h>
#include <string.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_siman.h>
#include <gsl/gsl_randist.h>

#include "symbolic.c"

#define J_END_COL(i) \
  for (j=0; j<PN; j++) gsl_matrix_set(J,i,j,J_COL[j]);

static inline _Bool IS_SMALL(double v) {
  return v < 1E6;
}

static inline double sinc(double x) {
  return gsl_sf_sinc(x / M_PI);
}

static int NP;
static double *INPUT; /* dyanmic array, on main's stack */
static double PREP[NPREP];

static void printcore(const double *X) {
  int i, j;
  DECLARE_F_G;
  CALCULATE_F_G;
  printf("[");
  for (i=0; i<NP; i++)
    for (j=0; j<3; j++)
      printf(" %25.18g,", POINT(i)[j]);
  printf(" ]\n");
}

static void prepare(double X[] /* startpoint */) {
  /* fills in PREP and startpoint */
  PREPARE;
}

//#define DEBUG

static double cb_Efunc(void *xp) {
  const double *X = xp;
  int P;
  DECLARE_F_G;
  CALCULATE_F_G;

#ifdef DEBUG
  int i,j;
  printf(" Efunc\n");
  for (j=0; j<3; j++) {
    for (P=0; P<NP; P++)
      printf(" %7.4f", POINT(P)[j]);
    printf("\n");
  }
  printf("             ");
#endif

  CALCULATE_COST;
  return cost;
}

static void cb_step(const gsl_rng *rng, void *xp, double step_size) {
  double *x = xp;
  int i;
  double step[NX];
  gsl_ran_dir_nd(rng, NX, step);
  for (i=0; i<NX; i++)
    x[i] += step_size * step[i];
  //printf("\n cb_step %p %10.7f [", xp, step_size);
  //for (i=0; i<N; i++) printf(" %10.7f,", step[i]);
  //printf("]\n");
}

static double cb_metric(void *xp, void *yp) {
  const double *x=xp, *y=yp;
  int i;
  double s;
  for (i=0, s=0; i<NX; i++) {
    double d = x[i] - y[i];
    s += d*d;
  }
  return sqrt(s);
}

static void __attribute__((unused)) cb_print(void *xp) {
  const double *x = xp;
  printf("\n");
  printcore(x);
}

static double scan1double(void) {
  double v;
  int r;

  r = scanf("%lf",&v);
  if (ferror(stdin)) { perror("stdin"); exit(-1); }
  if (feof(stdin)) exit(0);
  if (r!=1) { fputs("bad input\n",stderr); exit(-1); }
  return v;
}

int main(int argc, const char *const *argv) {
  double epsilon;
  int i;

  NP = atoi(argv[1]);
  epsilon = atof(argv[2]);

  gsl_rng *rng = gsl_rng_alloc(gsl_rng_ranlxd2);

  double input[NINPUT]; INPUT = input;
  double startpoint[NX];

  for (;;) {
    /* NINPUT + 1 doubles: startpoint, epsilon for residual */
    for (i=0; i<NINPUT; i++)
      INPUT[i] = scan1double();

    gsl_rng_set(rng,0);

    gsl_siman_params_t siman_params = {
      .k = 1.0,
      .t_initial = 0.5,
      .mu_t = 1.001,
      .t_min = epsilon * 1E-3,
      .iters_fixed_T = 100,
      .n_tries = 10,
      .step_size = 0.05,
    };

    prepare(startpoint);

    gsl_siman_solve(rng,
		    startpoint,
		    cb_Efunc, cb_step, cb_metric,
		    0, // cb_print,
		    0,0,0, sizeof(startpoint), siman_params);

    printcore(startpoint);

    printf("[]\n");
    fflush(stdout);
  }
}
