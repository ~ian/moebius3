
from __future__ import print_function

import multiprocessing

from moedebug import *

class ScheduledTask():
  avail = multiprocessing.cpu_count()
  running = [ ]
  factor = 0.74

  @staticmethod
  def tidy_queue():
    ScheduledTask.running = [
      st
      for st in ScheduledTask.running
      if st.running
    ]

  @staticmethod
  def want(weight):
    while True:
      ScheduledTask.tidy_queue()
      running_weight = sum(map((lambda st: st.weight), ScheduledTask.running))
      if ScheduledTask.avail - running_weight >= weight:
        return
      if not ScheduledTask.running:
        return
      other = ScheduledTask.running[0]
      other._dbg('run_now!')
      other.run_now()
      assert(not other.running)

  def _dbg(st, s):
    dbg('ST %s %s' % (st.desc, s))

  def __init__(st, run_now, desc, weight=1.):
    st.run_now = run_now
    st.weight = weight * ScheduledTask.factor
    st.running = False
    st.desc = desc
    st._dbg('create')

  def pre_spawn(st):
    st._dbg('pre_spawn')
    assert(not st.running)
    ScheduledTask.want(st.weight)

  def post_spawn(st):
    st._dbg('post_spawn')
    st.running = True
    ScheduledTask.running.append(st)
    ScheduledTask.want(0)

  def post_reap(st):
    st._dbg('post_reap')
    st.running = False
