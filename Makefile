
all: moebius-core.scad moebius-mesh.scad

PLAY = /home/reprap/play
USING_AUTOS = moebius

COPTIMISE = -O2
CDEBUG = -g
CWARNINGS = -Wall -Wwrite-strings -Wshadow -Wextra -Wno-unused-parameter
CFLAGS = $(COPTIMISE) $(CWARNINGS) $(CDEBUG)

$(shell set -xe; $(PLAY)/commitid.scad.pl >commitid.scad.tmp; cmp commitid.scad.tmp commitid.scad || mv -f commitid.scad.tmp commitid.scad )

PYLIBS = $(shell echo *.py)

moebius-core.scad: genscad $(PYLIBS)
		./$< >$@.tmp && mv -f $@.tmp $@

moebius-mesh.scad: meshscad $(PYLIBS)
		./$< >$@.tmp && mv -f $@.tmp $@

AUTO_TOPLEVELS := $(foreach m,$(USING_AUTOS),$(shell $(PLAY)/toplevel-find $m))

symbolic.c:	gensymbolic symbolic.py
		./gensymbolic -q >$@.tmp && mv -f $@.tmp $@

findcurve.o:	findcurve.c symbolic.c

GSL_LIBS := $(shell pkg-config --libs gsl)

findcurve:	findcurve.o $(GSL_LIBS)

autoincs:	$(AUTO_INCS)
scads:		$(addsuffix .auto.scad, $(AUTO_TOPLEVELS))
stls:		$(addsuffix .auto.stl, $(AUTO_TOPLEVELS))

%.auto.scads: %.scad
	$(MAKE) $(addsuffix .auto.scad, $(shell $(PLAY)/toplevel-find $*))
%.auto.stls:
	$(MAKE) $(addsuffix .auto.stl, $(shell $(PLAY)/toplevel-find $*))

.PRECIOUS: %.auto.scad
%.auto.scad: $(PLAY)/toplevel-make Makefile $(PLAY)/toplevel-find
		@echo ' write $@'
		$< $@ >$@.tmp
	        @mv -f $@.tmp $@

.PRECIOUS:	%.stl %.gcode %.eps %.dxf

