
from __future__ import print_function

import numpy as np

tau = np.pi * 2

origin = np.array((0,0,0))
unit_x = np.array((1,0,0))
unit_y = np.array((0,1,0))
unit_z = np.array((0,0,1))

def unit_v(v):
  return v / np.linalg.norm(v)

def augment(v, augwith=1): return np.append(v, augwith)
def augment0(v): return augment(v, 0)
def unaugment(v): return v[0:3]

def matmultiply(mat,vect):
  # both are "array"s
  # we would prefer to write   mat @ vect
  # but that doesn't work in Python 2
  return np.array((vect * np.matrix(mat).T))[0,:]

def matmatmultiply(mat1,mat2):
  # both are "array"s
  # we would prefer to write   mat1 @ mat2
  # but that doesn't work in Python 2
  return np.array((np.matrix(mat1) * np.matrix(mat2)))

def augmatmultiply(mat,unaugvect, augwith=1):
  return unaugment(matmultiply(mat, augment(unaugvect, augwith)))

