
from __future__ import print_function

import moenp

_files = []

def dbg_file(f):
  _files.append(f)

def dbg_enabled():
  return not not _files

def dbg(*args):
  for f in _files:
    s = ' '.join(map(str,args))
    for l in s.split('\n'):
      print('// D ', l, file=f)
    f.flush()

def vec2dbg(v): return '[%6.3f %6.3f %6.3f]' % tuple(v)

class BaseVisdebug():
  def line(vd, p0, p1, **kw):
    vd.curve(lambda t: p0 + t * (p1-p0), **kw)
  def arrow(vd, p, d, **kw):
    vd.line(p, p+d, **kw)
  def basis(vd, basis, hue=(1,1,1)):
    for ax in range(0,3):
      vd.arrow(basis[0:3,3], basis[0:3,ax],
               color= tuple([ c * (1.0 - ax*0.25) for c in hue ]))

class NullVisdebug():
  def curve(*a, **kw): pass
  def line(*a, **kw): pass
  def circle(*a, **kw): pass
  def arrow(*a, **kw): pass
  def basis(*a, **kw): pass
  def crashing(*a, **kw): pass

class MatrixVisdebug(BaseVisdebug):
  def __init__(m, base, matrix):
    m._b = base
    m._matrix = matrix
  def curve(m, fn, **kw):
    m._b.curve(lambda t: moenp.augmatmultiply(m._matrix, fn(t)), **kw)
  def circle(m, c, axis, **kw):
    m._b.circle(moenp.augmatmultiply(m._matrix, c),
                moenp.augmatmultiply(m._matrix, axis, 0),
                **kw)
  def arrow(m, p, d, **kw):
    m._b.arrow(moenp.augmatmultiply(m._matrix, p),
               moenp.augmatmultiply(m._matrix, d, 0),
               **kw)
  def crashing(m, *a, **kw): m._b.crashing(*a, **kw)

_nullvis = NullVisdebug()
_vis = [ _nullvis ]

def vdbg():
  return _vis[0]

def vdbg_enabled():
  return _vis[0] != _nullvis

def vdbg_enable(vis):
  _vis[0] = vis
